import { shallowMount, mount } from '@vue/test-utils';
import Home from '@/views/Home.vue';

describe('home.vue', () => {
  it('renders the component with shollow mount', () => {
    const wrapper = shallowMount(Home, {});
    expect(wrapper).toMatchSnapshot();
  });
  it('renders the component with mount', () => {
    const wrapper = mount(Home, {});
    expect(wrapper).toMatchSnapshot();
  });
});

import { mount, shallowMount } from '@vue/test-utils';
import ChildComp from '@/components/ChildComp.vue';
import Counter from '@/components/Counter.vue';

describe('Counter.vue', () => {
  // testing props, idem pour les data.
  it('props are well received', () => {
    const wrapper = mount(Counter, { propsData: { value: 22 } });
    expect(wrapper.vm.value).toBe(22);
    expect(wrapper.props().value).not.toBe(21);
    wrapper.setProps({ value: 12 });
    expect(wrapper.vm.value).toBe(12);
  });

  // testing methods
  it('increment methode should work', () => {
    const wrapper = mount(Counter, {});
    wrapper.vm.increment();
    expect(wrapper.vm.counter).toBe(1);
  });
  it('decrement methode should work', () => {
    const wrapper = mount(Counter, {});
    wrapper.setData({ counter: 5 });
    wrapper.vm.decrement();
    expect(wrapper.vm.counter).toBe(4);
  });

  // testing event
  it('click on increment button should increment the count', () => {
    const wrapper = mount(Counter, { propsData: { value: 3 } });
    const button = wrapper.findAll('button');
    button.at(0).trigger('click');
    // wrapper.find(button.increment)
    expect(wrapper.vm.counter).toBe(4);
  });
  it('click on decrement button should decrement the count', () => {
    const wrapper = mount(Counter, { propsData: { value: 4 } });
    const button = wrapper.findAll('button');
    button.at(1).trigger('click');
    // wrapper.find(button.decrement)
    expect(wrapper.vm.counter).toBe(3);
  });

  // wrting snapshots
  it('counter snpashot without emiting child event', () => {
    const wrapper = shallowMount(Counter, {});
    expect(wrapper).toMatchSnapshot();
  });
  it('counter snpashot with emiting child event', () => {
    const wrapper = shallowMount(Counter, {});
    wrapper.find(ChildComp).vm.$emit('customEvent');
    expect(wrapper.findAll(ChildComp).length).toBe(0);
    expect(wrapper).toMatchSnapshot();
  });
});
